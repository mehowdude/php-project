<?php
	class searching{
	
		///This array is going to contain all the directories.
		var $directories = array("./");
		
		///lenght of the potential line
		var $lengthsOfVirus = 1000;
		
		///This array contains all the key phrases.
		var $names = array('<?php $janbbl = ');
		
		/*/
		* This function write results of potential viruses.
		* @param $nameOfFile-> the file in which contains the potential virus.
		/*/
		
		private function savingResults($nameOfFile){
			
			$file = fopen("./potentialViruses.txt","r");
			$nothing = 0;
			while (($line=fgets($file))!==False){
				if (strpos($line,$nameOfFile)!==False) {
						$nothing = 1;
						break; ///the same potential virus was found.
					}
				}
			fclose($file);
			
			///different potential virus was found.
			
			if ($nothing == 0){
					$file = fopen("./potentialViruses.txt","a+");
					fwrite($file,"potential virus in ".$nameOfFile.PHP_EOL);
					fclose($file);
				}
			}
				
				
		/*/
		* This function rewrites the infected files.
		* @param $source-> file which has to be deleted.
		* @param $lineToDelete-> line which contains the virus.
		/*/
		
		private function deletion($source){
			
			///creating temporary file to which file is rewritten without the virus.
			
			$target=$source.'.txt';
			$first = 0;
			
			$readFile=fopen($source, 'r');
			$writeFile=fopen($target, 'w');
			
			///copy operation
			$writeFile=fopen($target, 'w');
			while (($line=fgets($readFile))!==False){
				if ($first == 0) {
					$line = preg_replace('/^<[?]php.*[?]>/', '', $line); ///deleting the part of the line which contains the virus.
					fwrite($writeFile, $line);
					$first = 1;
					}
				else{
					fwrite($writeFile, $line);
					}
				}

			fclose($readFile);
			fclose($writeFile);
			unlink($source);
			rename($target, $source);
			}
		
		
		
		/*/
		* This function reads the first line of the php file.
		* @param $nameOfFile -> this is the current file.
		/*/
		
		
		private function openingFile($nameOfFile){
			$file = fopen($nameOfFile,"r");
			$line = fgets($file);
			$nothing = 0;
			foreach($this->names as $key){
				if (strpos($line,$key)!==false){
					fclose($file);
					$this->deletion($nameOfFile); ///deletion happens here.
					$nothing=1;
					break; ///the virus was found.
					}
				}
				///if no virus was discovered in file then it will check if there is a potential virus, and it will close the file.
				if($nothing == 0){ 	
					///checking for potential virus.
					if(strlen($line)>$this->lengthsOfVirus){
						$this->savingResults($nameOfFile);
						}
					fclose($file);
					}
				}
			
		
		/*/
		* This function finds all the php files in the currect directory.
		* @param $directory -> this is the current directory.
		/*/
		
		private function getFiles($nameOfDirectory){ 
			$files = glob($nameOfDirectory."*.php");
			foreach($files as $name){
				$this->openingFile($name);
				}
			}
		
			
		/*/
		* This function finds all the directories in the currect directory.
		* @param $nameOfDirectory -> this is the current directory.
		/*/	
			
		private function getDirectory($nameOfDirectory){
				$tempDirectories = array_values(array_filter(glob($nameOfDirectory.'*'),'is_dir'));
				foreach($tempDirectories as $key){
					array_push($this->directories,$key.'/');
					}
			}
		
		
		
		/*/
		* This is the main function of the class.
		* Here all the functions which are responsible for
		* finding the virus are implemented.
		/*/
		
		public function core(){
			
			///checking if text file exists and creates one if it doesnt exist.
			
			if(!file_exists("./potentialViruses.txt")){
				$file = fopen("./potentialViruses.txt","w");
				fclose($file);
				}
			
			///The program is getting all the directories and inserting it into $directories.
			
			for($i = 0; $i<count($this->directories); $i++){
				$this->getDirectory($this->directories[$i]);
				}			
			
			///Checking the files in the directories.
			
			foreach($this->directories as $keys){
					$this->getFiles($keys);
				}
			}
		}
		
	$obj = new searching();
	$obj->core();
?>

